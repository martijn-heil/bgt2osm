# bgt2osm Een programma om BGT data te downloaden en om te zetten naar OSM XML.
# Copyright (C) 2020  Martijn Heil
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

plusfvwMappings = { # plus_fysiekVoorkomenWegdeel to surface=*
  'asfalt': 'asphalt',
  'betonstraatstenen': 'paving_stones',
  'zand': 'sand',
  'tegels': 'paving_stones',
}

sfmMappings = {
  'open verharding': 'paved',
  'gesloten verharding': 'paved'
}

def surface(attrs):
  pfvw = attrs.get('plus_fysiekVoorkomenWegdeel')
  sfm = attrs.get('surfaceMaterial')
  if pfvw != None:
    return plusfvwMappings.get(pfvw)
  elif sfm != None:
    return sfmMappings.get(sfm)
  else:
    return None

def filterTags(attrs):
  if not attrs:
    return

  tags = {}
  if attrs.get('class') == 'grasland agrarisch':
    tags['landuse'] = 'meadow'
    tags['meadow'] = 'agricultural'
  elif attrs.get('class') == 'gemengd bos':
    tags['landuse'] = 'forest'
  elif attrs.get('class') == 'bouwland':
    tags['landuse'] = 'farmland'
  elif attrs.get('class') == 'waterloop':
    tags['natural'] = 'water'
  elif attrs.get('class') == 'watervlakte':
    tags['natural'] = 'water'
    if attrs.get('plus_type') == 'sloot':
      tags['water'] = 'ditch'
  elif attrs.get('class') == 'groenvoorziening':
    if attrs.get('plus_fysiekVoorkomen') == 'heesters' or attrs.get('plus_fysiekVoorkomen') == 'struikrozen':
      tags['barrier'] = 'hedge'
      tags['area'] = 'yes'
    elif attrs.get('plus_fysiekVoorkomen') == 'gras- en kruidachtigen':
      tags['landuse'] = 'grass'
    elif attrs.get('plus_fysiekVoorkomen') == 'bosplantsoen':
      tags['landuse'] = 'forest'
  elif attrs.get('class') == 'grasland overig':
    tags['landuse'] = 'grass'
  elif attrs.get('function') == 'rijbaan lokale weg':
    tags['area:highway'] = 'road'
  elif attrs.get('function') == 'rijbaan regionale weg':
    tags['area:highway'] = 'road'
  elif attrs.get('function') == 'voetpad':
    tags['area:highway'] = 'footway'
  elif attrs.get('function') == 'parkeervlak':
    tags['amenity'] = 'parking'
  elif attrs.get('function') == 'verkeerseiland':
    tags['area:highway'] = 'traffic_island'
  elif attrs.get('function') == 'inrit':
    tags['area:highway'] = 'service'
  elif attrs.get('function') == 'fietspad':
    tags['area:highway'] = 'cycleway'
  elif attrs.get('function') == 'berm' and attrs.get('plus_fysiekVoorkomenOndersteunendWegdeel') == 'gras- en kruidachtigen':
    tags['landuse'] = 'grass'
  elif attrs.get('function') == 'voetpad op trap':
    tags["area:highway"] = 'steps'


  #if tags == {}: # Don't remove tags when no real conversion is found
  #  return attrs
  for k, v in attrs.items():
      tags["|BGT:" + k] = v

  layer = attrs.get('relatieveHoogteligging')
  if layer != '0':
    tags['layer'] = layer
  surfaceval = surface(attrs)
  if surfaceval != None:
    tags['surface'] = surfaceval
  return tags
