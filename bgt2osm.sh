#!/bin/bash
#
# bgt2osm Een programma om BGT data te downloaden en om te zetten naar OSM XML.
# Copyright (C) 2020  Martijn Heil
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
#
#
# Geschreven voor bash op linux.
#
# externe dependencies: ogr2osm, gdal, ogr2osm-bgttrans
# linux dependencies: rm, mkdir, tail, sed, enhanced getopt, mktemp, wget, unzip
# and bash, obviously.

# Saner programming env: these switches turn some bugs into errors
set -o errexit -o pipefail -o noclobber -o nounset

VERSION="0.1.0"
PROGRAMNAME="osmdata.sh"


function print_help {
  echo "\
$PROGRAMNAME $VERSION
Martijn Heil <m.heil375@gmail.com>
Copyright (c) 2020 Martijn Heil

Bash script om BGT gegevens om te zetten naar OSM XML.

USAGE:
    $PROGRAMNAME [FLAGS] [OPTIONS]

FLAGS:
    -h, --help        Prints help information
    -v, --version     Prints version information

OPTIONS:
    -o, --output <FILE>             Pad naar output ZIP-bestand. Bijvoorbeeld: 'output.zip'. Wanneer dit ongespecificeerd wordt
                                      gelaten zal het ZIP-bestand naar stdout worden geschreven.
    --bounding-polygon <WKT>        Bounding Well-Known Text (WKT) polygon
    --bounding-polygon-file <FILE>  Pad naar bestand waarvan de inhoud de Well-Known Text (WKT) bounding polygon is."
}

# Check if enhanced getopt is available
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
  echo "I'm sorry, `getopt --test` failed in this environment." >> /dev/stderr
  echo "This probably means that there is no enhanced getopt available." >> /dev/stderr
  exit 1
fi

OPTIONS=t:o:h
LONGOPTS=translation:,output:,help,bounding-polygon:,bounding-polygon-file:

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  exit 2
fi

eval set -- "$PARSED"
# bounding polygon in Well Known Text (WKT), de coordinaten in RD
BOUNDING_POLYGON=-
BOUNDING_POLYGON_FILE=-
OUTPUT_FILE=-
TRANSLATION=-
f=n

while true; do
  case "$1" in
    -t|--translation)
    TRANSLATION="$2"
    shift 2
    ;;

    -o|--output)
    OUTPUT_FILE="$2"
    shift 2
    ;;

    --bounding-polygon)
    BOUNDING_POLYGON=$2
    shift 2
    ;;

    --bounding-polygon-file)
    BOUNDING_POLYGON_FILE=$2
    shift 2
    ;;

    -h|--help)
    print_help
    exit 0
    ;;

    --)
    shift
    break
    ;;

    *)
    echo "Programming error" >> /dev/stderr
    exit 3
    ;;
  esac
done


if [ $TRANSLATION = '-' ]; then
  TRANSLATION=/lib/bgttrans.py
else if [ -e "$TRANSLATION" ]; then
  TRANSLATION=$(realpath "$TRANSLATION")
  echo "Using translation: '$TRANSLATION'" >> /dev/stderr
fi
fi

if [ $OUTPUT_FILE != '-' ]; then
  OUTPUT_FILE=$(realpath "$OUTPUT_FILE")
fi

if [ $BOUNDING_POLYGON_FILE != '-' ]; then
  BOUNDING_POLYGON=$(cat "$BOUNDING_POLYGON_FILE")
fi


DIR=$(mktemp -d)

# Gebruikt op specifieke manier afgeronde versie van EPSG:4833 voor de +towgs84 parameter,
# exact dezelfde als in de BAG-import plugin. Het is dus belangrijk dat deze nooit veranderd wordt.
RD="+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs"

rm -rf "$DIR"
set -e # Exit if any command fails
set -f # Disable glob
mkdir -p "$DIR"
cd "$DIR"

mkdir tmp

# Setup done..
echo "Initializatie compleet." >> /dev/stderr



mkdir tmp/BGT
echo "<OGRVRTDataSource>
  <OGRVRTLayer name=\"wegdeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_wegdeel.gml</SrcDataSource>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcLayer>TrafficArea</SrcLayer>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"ondersteunendwegdeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_ondersteunendwegdeel.gml</SrcDataSource>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcLayer>AuxiliaryTrafficArea</SrcLayer>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"begroeidterreindeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_begroeidterreindeel.gml</SrcDataSource>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcLayer>PlantCover</SrcLayer>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"onbegroeidterreindeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_onbegroeidterreindeel.gml</SrcDataSource>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"pand\">
    <SrcDataSource>tmp/BGT/gml/bgt_pand.gml</SrcDataSource>
    <SrcLayer>BuildingPart</SrcLayer>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"waterdeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_waterdeel.gml</SrcDataSource>
    <SrcLayer>waterdeel</SrcLayer>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>

  <OGRVRTLayer name=\"ondersteunendwaterdeel\">
    <SrcDataSource>tmp/BGT/gml/bgt_ondersteunendwaterdeel.gml</SrcDataSource>
    <SrcLayer>ondersteunendwaterdeel</SrcLayer>
    <LayerSRS>EPSG:28992</LayerSRS>
    <SrcRegion>$BOUNDING_POLYGON</SrcRegion>
    <GeometryField name=\"geom\" />
  </OGRVRTLayer>
</OGRVRTDataSource>" > tmp/BGT/bgt.vrt

mkdir tmp/BGT/gml
cd tmp/BGT/gml
#wget -O extract.zip 'https://downloads.pdok.nl/service/extract.zip?extractname=bgt&extractset=citygml&excludedtypes=plaatsbepalingspunt&history=false&tiles=%7B%22layers%22%3A%5B%7B%22aggregateLevel%22%3A0%2C%22codes%22%3A%5B27468%2C27465%2C27464%2C27423%2C27421%2C27422%2C27444%2C27441%2C27443%2C27442%2C27448%2C27437%2C27439%2C27525%2C27524%2C27527%2C27538%2C27545%2C27544%2C27548%2C27550%2C27551%2C27549%2C27547%2C27592%2C27594%2C27595%2C27496%2C27490%2C27491%2C27455%2C27586%2C27543%2C27541%2C27540%2C27542%2C27537%2C27536%2C27539%2C27451%2C27450%2C27454%2C27449%2C27452%2C27453%2C27447%2C27446%2C27445%2C27488%2C27489%2C27467%2C27466%2C27470%2C27593%5D%7D%5D%7D'
cp "$HOME/extract.zip" extract.zip 1>&2
unzip extract.zip 1>&2
cd "$DIR"

echo "BGT Gegevens filteren op interessegebied.." >> /dev/stderr
# ogr2osm doesn't handle CurvePolygons and our other exotic stuff very well, so first we convert it into a simplified GML file
ogr2ogr -skipfailures tmp/BGT/bgt_filtered.gml tmp/BGT/bgt.vrt 1>&2


echo "BGT omzetten naar OSM XML.." >> /dev/stderr

if [ -e $TRANSLATION ]; then
  cp $TRANSLATION trans.py 1>&2
  TRANSLATION="trans.py"
fi

if [ $OUTPUT_FILE != '-' ]; then
  ogr2osm tmp/BGT/bgt_filtered.gml --never-download --no-upload-false -p "$RD" --significant-digits=7 -t "$TRANSLATION" -o "$OUTPUT_FILE"
else
  ogr2osm tmp/BGT/bgt_filtered.gml --never-download --no-upload-false -p "$RD" --significant-digits=7 -t "$TRANSLATION" -o "output.osm"
  cat output.osm
fi

set +f # Re-enable glob
echo "Opschonen.." >> /dev/stderr
rm -rf "$DIR"
